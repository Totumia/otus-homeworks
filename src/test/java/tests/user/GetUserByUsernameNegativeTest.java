package tests.user;

import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.Test;
import services.UserApi;


public class GetUserByUsernameNegativeTest {
  /*
     1.Получение данных с инвалидным именем пользователя
     2.Проверка статус кода (404)
     3.Проверка {code,type,message} из тела ответа
    */
  @Test
  public void checkGetUserByIncorrectUsername() {
    UserApi userApi = new UserApi();
    userApi.getNonExistentUser("nouserwiththisusername")
        .body("type", equalTo("error"))
        .body("message", equalTo("User not found"))
        .body("code", equalTo(1));

  }

}
