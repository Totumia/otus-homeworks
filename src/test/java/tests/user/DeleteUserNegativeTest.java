package tests.user;

import dto.user.request.DtoUser;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.UserApi;


public class DeleteUserNegativeTest {
  private UserApi userApi;
  private DtoUser user;

  @BeforeTest
  public void setUp() {
    userApi = new UserApi();
  }

  /*
     1.Проверка,что пользователь реально не существует
     2.Удаление несуществующего пользователя
     3.Проверка статус кода (404)
    */
  @Test
  public void deleteUser() {
    userApi.getNonExistentUser("notexistentuser777");
    userApi.deleteNonExistentUser("notexistentuser777");
  }
}
