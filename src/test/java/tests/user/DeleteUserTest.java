package tests.user;

import static org.hamcrest.Matchers.equalTo;

import dto.user.request.DtoUser;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.UserApi;


public class DeleteUserTest {
  private UserApi userApi;
  private DtoUser user;

  @BeforeTest
  public void setUp() {
    userApi = new UserApi();
    user = DtoUser.builder()
        .email("userForDelete@mailforspam.com")
        .firstName("Deleted")
        .id(87651)
        .lastName("Deleted")
        .password("delete")
        .phone("+66677457888")
        .userStatus(895334)
        .username("del")
        .build();
    userApi.createUser(user);
  }

  /*
     1.Удаление существующего пользователя
     2.Вызов метода getNonExistentUser(для удостоверения,что данный пользователь удален)
     2.Проверка статус кода (404)
     3.Проверка {code,type,message} из тела ответа
    */
  @Test
  public void deleteUser() {
    userApi.deleteUser("del");
    userApi.getNonExistentUser("del")
        .body("type", equalTo("error"))
        .body("message", equalTo("User not found"))
        .body("code", equalTo(1));
  }
}
