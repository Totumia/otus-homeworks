package tests.user;

import static org.hamcrest.Matchers.equalTo;

import dto.user.request.DtoUser;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import services.UserApi;


public class CreateNewUserTest {
  private UserApi userApi;
  private DtoUser user;

  @BeforeTest
  public void setUp() {
    userApi = new UserApi();
  }

  /*
  1.Создание пользователя со всеми параметрами
  2.Проверка статус кода (200)
  3.Проверка того,что созданный пользователь существует и его данные попали правильно
 */
  @Test
  public void checkCreateNewUser() {
    user = DtoUser.builder()
        .email("tofig@mailforspam.com")
        .firstName("Tofig")
        .id(555)
        .lastName("Hagverdov")
        .password("test123")
        .phone("+666777888")
        .userStatus(111)
        .username("totumia")
        .build();
    userApi.createUser(user);
    userApi.getUser("totumia")
        .body("firstName", equalTo(user.getFirstName()))
        .body("lastName", equalTo(user.getLastName()))
        .body("password", equalTo(user.getPassword()))
        .body("userStatus", equalTo(user.getUserStatus()))
        .body("phone", equalTo(user.getPhone()))
        .body("id", equalTo(user.getId()))
        .body("email", equalTo(user.getEmail()))
        .body("username", equalTo(user.getUsername()));
  }

}


