package services;

import static io.restassured.RestAssured.given;

import dto.user.request.DtoUser;
import io.restassured.response.ValidatableResponse;
import specifications.Specifications;


public class UserApi {
  public static final String CREATE_USER = "/user";
  public static final String GET_USER_BY_USERNAME = "/user/{username}";
  public static final String DELETE_USER = "/user/{username}";

  public ValidatableResponse createUser(DtoUser user) {
    Specifications.installSpecification(Specifications.requestSpecification(), Specifications.responseSpec200Status());
    return given()
        .body(user)
        .when()
        .post(CREATE_USER)
        .then()
        .log().all();
  }

  public ValidatableResponse getUser(String username) {
    Specifications.installSpecification(Specifications.requestSpecification(), Specifications.responseSpec200Status());
    return given()
        .pathParams("username", username)
        .when()
        .get(GET_USER_BY_USERNAME)
        .then()
        .log().all();
  }

  public ValidatableResponse deleteUser(String username) {
    Specifications.installSpecification(Specifications.requestSpecification(), Specifications.responseSpec200Status());
    return given()
        .pathParams("username", username)
        .when()
        .delete(DELETE_USER)
        .then()
        .log().all();
  }

  public ValidatableResponse deleteNonExistentUser(String username) {
    Specifications.installSpecification(Specifications.requestSpecification(), Specifications.responseSpec404Status());
    return given()
        .pathParams("username", username)
        .when()
        .delete(DELETE_USER)
        .then()
        .log().all();
  }

  public ValidatableResponse getNonExistentUser(String username) {
    Specifications.installSpecification(Specifications.requestSpecification(), Specifications.responseSpec404Status());
    return given()
        .pathParams("username", username)
        .when()
        .get(GET_USER_BY_USERNAME)
        .then()
        .log().all();
  }

}
