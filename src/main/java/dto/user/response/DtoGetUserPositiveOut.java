package dto.user.response;

import lombok.Data;

@Data
public class DtoGetUserPositiveOut {
  private String firstName;
  private String lastName;
  private String password;
  private Integer userStatus;
  private String phone;
  private Long id;
  private String email;
  private String username;
}