package specifications;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;


public class Specifications {
  public static RequestSpecification requestSpecification() {
    return new RequestSpecBuilder()
        .setBaseUri(System.getProperty("base.url"))
        .setContentType(ContentType.JSON)
        .log(LogDetail.ALL)
        .build();
  }

  public static ResponseSpecification responseSpec200Status() {
    return new ResponseSpecBuilder()
        .expectStatusCode(200)
        .build();
  }

  public static ResponseSpecification responseSpec404Status() {
    return new ResponseSpecBuilder()
        .expectStatusCode(404)
        .build();
  }

  public static void installSpecification(RequestSpecification reqSpec, ResponseSpecification respSpec) {
    RestAssured.requestSpecification = reqSpec;
    RestAssured.responseSpecification = respSpec;
  }


}
